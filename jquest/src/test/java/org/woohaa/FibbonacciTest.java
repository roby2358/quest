package org.woohaa;

import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static org.testng.Assert.assertEquals;

public class FibbonacciTest {

    private Map<Integer, Integer> cache = new HashMap<>();

    private int fibonacci(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;

        int a = 1;
        int b = 1;
        for (int i = 2 ; i < n ; i++) {
            int c = b;
            b += a;
            a = c;
        }
        return b;
    }

    private int fibonacciRecur(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;

        return fibonacciRecur(n - 1) + fibonacciRecur(n - 2);
    }

    private int fibonacciCached(int n) {
        if (cache.containsKey(n)) return cache.get(n);
        if (n == 0) return 0;
        if (n == 1) return 1;

        int v = fibonacciCached(n - 1) + fibonacciCached(n - 2);
        cache.put(n, v);
        return v;
    }

    @Test
    void verify() {
        verify(this::fibonacci);
    }

    @Test
    void verifyRecur() {
        verify(this::fibonacciRecur);
    }

    @Test
    void verifyRecurCached() {
        verify(this::fibonacciCached);
    }

    private void verify(Function<Integer, Integer> f) {
        assertEquals(0, (int) f.apply(0));
        assertEquals(1, (int) f.apply(1));
        assertEquals(1, (int) f.apply(2));
        assertEquals(2, (int) f.apply(3));
        assertEquals(3, (int) f.apply(4));
        assertEquals(5, (int) f.apply(5));
        assertEquals(8, (int) f.apply(6));
        assertEquals(610, (int) f.apply(15));
        assertEquals(6765, (int) f.apply(20));
    }
}
