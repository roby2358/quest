package org.woohaa;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.testng.Assert.*;


public class BinarySearchPredicateTest {

    private static final int MAX = 10;

    private static final Random rr = new Random();

    private Thing random() {
        int[] rand = Arrays.stream(new int[MAX]).map((int i) -> rr.nextInt(MAX)).sorted().toArray();
        return new Thing(rand);
    }

    private class Thing implements Predicate<Integer> {
        private final int[] value;

        Thing(int... v) {
            this.value = Arrays.stream(v).limit(MAX).toArray();
        }

        public boolean test(Integer i) {
            return value[i] < 4;
        }
    }

    /**
     * @param p predicate for an int
     * @return the highest value of int that the predicate is true
     */
    private Optional<Integer> binarySearch(Predicate<Integer> p) {
        // min is inclusive, max is exclusive
        int min = 0;
        int max = MAX - 1;

        // Guard: make sure there's one at all
        if (!p.test(min)) return Optional.empty();
        if (p.test(max)) return Optional.of(max);

        while (min + 1 < max) {
            int n = (min + max) / 2;
            if (p.test(n))
                min = n;
            else
                max = n;
        }

        return Optional.of(min);
    }

    private Optional<Integer> binarySearchRecur(Predicate<Integer> p) {
        if (!p.test(0)) return Optional.empty();
        if (p.test(MAX - 1)) return Optional.of(MAX - 1);

        return Optional.of(search(p, 0, MAX));
    }

    private int search(Predicate<Integer> p, int min, int max) {
        if (min + 1 >= max) return min;

        int n = (min + max) / 2;
        return (p.test(n))
                ? search(p, n, max)
                : search(p, min, n);
    }

    @Test
    private void validateSearch() {
        verify(this::binarySearch);
    }

    @Test
    private void validateSearchRecur() {
        verify(this::binarySearchRecur);
    }

    private void verify(Function<Predicate<Integer>, Optional<Integer>> f) {
        assertTrue(new Thing(0, 0, 0, 0, 0, 0, 0, 0, 0, 0).test(0));
        assertFalse(binarySearch(new Thing(4, 4, 4, 4, 4, 4, 4, 4, 4, 4)).isPresent());
        assertEquals(0, (int) f.apply(new Thing(0, 4, 4, 4, 4, 4, 4, 4, 4, 4)).get());
        assertEquals(1, (int) f.apply(new Thing(0, 1, 4, 4, 4, 4, 4, 4, 4, 4)).get());
        assertEquals(2, (int) f.apply(new Thing(0, 1, 2, 4, 4, 4, 4, 4, 4, 4)).get());
        assertEquals(3, (int) f.apply(new Thing(0, 1, 2, 3, 4, 4, 4, 4, 4, 4)).get());
        assertEquals(4, (int) f.apply(new Thing(0, 1, 2, 3, 3, 4, 4, 4, 4, 4)).get());
        assertEquals(5, (int) f.apply(new Thing(0, 1, 2, 3, 3, 3, 4, 4, 4, 4)).get());
        assertEquals(6, (int) f.apply(new Thing(0, 1, 2, 3, 3, 3, 3, 4, 4, 4)).get());
        assertEquals(7, (int) f.apply(new Thing(0, 1, 2, 3, 3, 3, 3, 3, 4, 4)).get());
        assertEquals(8, (int) f.apply(new Thing(0, 1, 2, 3, 3, 3, 3, 3, 3, 4)).get());
        assertEquals(9, (int) f.apply(new Thing(0, 0, 0, 0, 1, 1, 1, 2, 2, 3)).get());
    }
}
