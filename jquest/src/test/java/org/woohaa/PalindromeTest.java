package org.woohaa;

import org.testng.annotations.Test;

import java.util.function.Predicate;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class PalindromeTest {

    private Boolean isPalindrome(String s) {
        if (s == null) return false;

        String cleaned = s.trim().toLowerCase();

        if (cleaned.isEmpty()) return false;

        char[] chars = cleaned.toCharArray();
        int length = chars.length;

        for (int i = 0 ; i < chars.length / 2 ; i++) {
            if (chars[i] != chars[length - i - 1]) return false;
        }

        return true;
    }

    private Boolean isPalindromeRecur(String s) {
        if (s == null) return false;

        String cleaned = s.trim().toLowerCase();

        if (cleaned.isEmpty()) return false;

        return check(cleaned);
    }

    private boolean check(CharSequence chars) {
        if (chars.length() < 2) return true;
        if (chars.charAt(0) != chars.charAt(chars.length() - 1)) return false;

        return check(chars.subSequence(1, chars.length() - 2));
    }

    @Test
    void testIsPalindrome() {
        verify(this::isPalindrome);
    }

    @Test
    void testIsPalindromeRecur() {
        verify(this::isPalindromeRecur);
    }

    void verify(Predicate<String> f) {
        assertFalse(f.test(null));
        assertFalse(f.test(""));
        assertFalse(f.test("      "));
        assertTrue(f.test("Bob"));
        assertTrue(f.test("    Bob"));
        assertTrue(f.test("Bob "));
        assertTrue(f.test("    Bob "));
        assertTrue(f.test("hAaH"));
        assertFalse(f.test(" Sam    "));
    }

}
