package org.woohaa;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MazeTest {

    @Test
    public void testOne() {
        MazeSolver t = new MazeSolver(new String(new char[]{MazeSolver.OPEN}), 1, 0).solve(0);
        t.print();
        Assert.assertTrue(t.getSolution().isPresent());
        Assert.assertEquals(t.getSolution().get().toString(), "[0]");
    }

    @Test
    public void testNone() {
        MazeSolver t = new MazeSolver(new String(new char[]{MazeSolver.BLOCKED, MazeSolver.BLOCKED}), 2, 1).solve(0);
        t.print();
        Assert.assertFalse(t.getSolution().isPresent());
    }

    @Test
    public void testEasy() {
        MazeSolver t = new MazeSolver(
                "---"
                        + "XX-"
                        + "XX-", 3, 8).solve(0);
        t.print();
        Assert.assertTrue(t.getSolution().isPresent());
        Assert.assertEquals(t.getSolution().get().toString(), "[0, 1, 2, 5, 8]");
    }


    @Test
    public void testHarder() {
        MazeSolver t = new MazeSolver(
                "-----"
                        + "-XXXX"
                        + "-----"
                        + "-XX-X"
                        + "--X--", 5, 24).solve(0);
        t.print();
        Assert.assertTrue(t.getSolution().isPresent());
        Assert.assertEquals(t.getSolution().get().toString(), "[0, 5, 10, 11, 12, 13, 18, 23, 24]");
    }

    @Test
    public void testBig() {
        int w = 21;
        int h = 21;
        int start = 0;
        int end = 21 * 21 - 1;
        MazeSolver t = new MazeSolver(new MazeBuilder(w, h, start, end).build().toString(), w, end).solve(start);
        t.print();
        Assert.assertTrue(t.getSolution().isPresent());
    }
}
