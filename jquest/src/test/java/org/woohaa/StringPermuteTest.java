package org.woohaa;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Set;
import java.util.stream.Collectors;

public class StringPermuteTest {

    private String toSortedString(Set<String> s) {
        return s.stream().sorted().collect(Collectors.toList()).toString();
    }
    @Test
    public void test1Permutations() {
        Set<String> s = new StringPermute("a").permute();
        Assert.assertEquals(toSortedString(s), "[a]");
    }

    @Test
    public void test2Permutations() {
        Set<String> s = new StringPermute("ab").permute();
        Assert.assertEquals(toSortedString(s), "[ab, ba]");
    }

    @Test
    public void test3Permutations() {
        Set<String> s = new StringPermute("abc").permute();
        Assert.assertEquals(toSortedString(s), "[abc, acb, bac, bca, cab, cba]");
    }

    @Test
    public void test3Combine2() {
        Set<String> s = new StringPermute("abc").combine(2);
        Assert.assertEquals(toSortedString(s), "[ab, ac, ba, bc, ca, cb]");
    }

    @Test
    public void test3Choose2() {
        Set<String> s = new StringPermute("abc").choose(2);
        Assert.assertEquals(toSortedString(s), "[ab, ac, bc]");
    }
}
