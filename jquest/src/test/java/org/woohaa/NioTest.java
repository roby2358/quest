package org.woohaa;

import org.testng.annotations.Test;

import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;

public class NioTest {

    private static final String EXPECTED = "WOO HAA";

    private static final String PATH = Paths.get("woo.txt").toAbsolutePath().toString();

    private ByteBuffer newByteBuffer() {
        return ByteBuffer.allocateDirect(4096);
    }

    private String readFile(String name) throws Exception {
        FileChannel channel = FileChannel.open(Paths.get(name), StandardOpenOption.READ);
        ByteBuffer buffer = newByteBuffer();
        channel.read(buffer);
        buffer.rewind();
        return StandardCharsets.UTF_8.decode(buffer).toString().trim();
    }

    private String putChars() throws Exception {
        ByteBuffer buf = newByteBuffer();
        buf.asCharBuffer().put(EXPECTED).rewind();
        return buf.asCharBuffer().toString().trim();
    }

    private String readLines(String name) throws Exception {
        return Files.lines(Paths.get(name)).collect(Collectors.toList()).get(0);
    }

    @Test
    void testReadFile() throws Exception {
        System.out.println(PATH);
        String got = readFile(PATH);
        System.out.println("[" + got + "]");
        assertEquals(got, EXPECTED);
    }

    @Test
    void testReadFileLines() throws Exception {
        System.out.println(PATH);
        String got = readLines(PATH);
        System.out.println("[" + got + "]");
        assertEquals(got, EXPECTED);
    }

    @Test
    void testReadBuffer() throws Exception {
        String got = putChars();
        System.out.println("[" + got + "]");
        assertEquals(got, EXPECTED);
    }

}
