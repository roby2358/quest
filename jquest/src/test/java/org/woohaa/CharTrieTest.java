package org.woohaa;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Optional;

public class CharTrieTest {

    CharTrie trie = new CharTrie();

    {
        trie.add("wooga");
        trie.add("wootanga");
        trie.add("zot");
    }

    @Test
    public void addCheckTest() {
        Assert.assertFalse(trie.check("haahaa"));
        Assert.assertTrue(trie.check("wooga"));
        Assert.assertTrue(trie.check("woo"));
        Assert.assertFalse(trie.check("woogat"));
        Assert.assertTrue(trie.check("wootanga"));
        Assert.assertTrue(trie.check("woota"));
        Assert.assertTrue(trie.check("zot"));
        Assert.assertTrue(trie.check("zo"));
        Assert.assertTrue(trie.check(""));
    }

    @Test
    public void checkNextSuccess() {
        String s = "wooga";
        Optional<CharTrie> tt = Optional.of(trie);

        int i = 0;
        for ( ; i < s.length() && tt.isPresent() ; i++) {
            tt = tt.get().checkNext(s.charAt(i));
        }
        Assert.assertEquals(i, s.length());
        Assert.assertTrue(i == s.length());
    }


    @Test
    public void checkNextFail() {
        String s = "woaga";
        Optional<CharTrie> tt = Optional.of(trie);

        int i = 0;
        for ( ; i < s.length() && tt.isPresent() ; i++) {
            tt = tt.get().checkNext(s.charAt(i));
        }
        Assert.assertEquals(i, 3);
        Assert.assertTrue(i < s.length());
    }
}
