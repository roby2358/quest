package org.woohaa;

import org.testng.annotations.Test;

public class MazeBuilderTest {

    @Test
    public void testSmallish() {
        MazeBuilder m = new MazeBuilder(21, 21, 0, 21 * 21 - 1).build();
        m.print();
    }
}
