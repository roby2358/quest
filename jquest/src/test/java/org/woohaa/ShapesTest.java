package org.woohaa;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.stream.IntStream;

public class ShapesTest {

    private static int EMPTY = 0;
    private static int UNCLAIMED = 0;

    /**
     * find contiguous (horizontal, vertical) shapes in an array denoted by 0s and 1s
     * e.g.
     * 0 1 0 0 1
     * 0 1 0 0 0
     * 0 1 0 1 0
     * 0 0 1 1 1
     * 0 0 1 1 0
     * 3 shapes: vert bar, singleton, fish
     */

    final int[] grid;
    final int[] contained;
    final int width;
    int shapeCount = 0;

    public ShapesTest() {
        this.grid = new int[0];
        this.contained = new int[0];
        this.width = 0;
        this.shapeCount = 0;
    }

    /**
     * @param grid a one dimensional array with each cell corresponding to y * width + x
     */
    public ShapesTest(int[] grid, int width) {
        this.grid = grid;
        this.width = width;
        this.contained = new int[grid.length];
        IntStream.range(0, this.contained.length).forEach(i -> this.contained[i] = 0);
    }

    public ShapesTest findShapes() {
        IntStream.range(0, grid.length)
                .forEach(i -> test(i, UNCLAIMED));
        return this;
    }

    private void test(int i, int shapeNumber) {
        if (i < 0 || i >= grid.length) {
            System.out.println("oob " + i + " " + shapeNumber);
            return;
        }

        if (grid[i] == EMPTY) {
            System.out.println("empty " + i + " " + shapeNumber);
            return;
        }

        if (contained[i] != UNCLAIMED) {
            System.out.println("claimed " + i + " " + shapeNumber);
            return;
        }

        int n = (shapeNumber == UNCLAIMED) ? ++shapeCount : shapeNumber;
        System.out.println("yes " + i + " " + shapeNumber + " -> " + n);
        contained[i] = n;

        // check neighbors
        test(i + width, n);
        test(i - width, n);

        if (i % width > 0) {
            test(i - 1, n);
        }

        if (i % width < width - 1) {
            System.out.println(n);
            test(i + 1, n);
        }
    }

    public int getShapeCount() {
        return shapeCount;
    }

    public void print() {
        final StringBuffer b = new StringBuffer(grid.length * 2);
        IntStream.range(0, grid.length).forEach(i -> {
                    b.append("" + grid[i]);
                    if (i % width == width - 1) b.append("\n");
                }
        );

        b.append("\n");

        IntStream.range(0, contained.length).forEach(i -> {
                    b.append("" + contained[i]);
                    if (i % width == width - 1) b.append("\n");
                }
        );
        System.out.println(b.toString());
    }

    @Test
    public void testOne() {
        ShapesTest t = new ShapesTest(new int[]{1}, 1).findShapes();
        t.print();
        Assert.assertEquals(t.getShapeCount(), 1);
    }

    @Test
    public void testNone() {
        ShapesTest t = new ShapesTest(new int[]{0}, 1).findShapes();
        Assert.assertEquals(t.getShapeCount(), 0);
    }

    @Test
    public void testBar() {
        ShapesTest t = new ShapesTest(new int[]{
                0, 1, 0,
                0, 1, 0,
                0, 1, 0}, 3).findShapes();
        t.print();
        Assert.assertEquals(t.getShapeCount(), 1);
    }

    @Test
    public void testBars() {
        ShapesTest t = new ShapesTest(new int[]{
                0, 1, 1,
                0, 0, 0,
                1, 1, 0}, 3).findShapes();
        t.print();
        Assert.assertEquals(t.getShapeCount(), 2);
    }

    @Test
    public void testCoding() {
        ShapesTest t = new ShapesTest(new int[]{
                0, 1, 0, 0, 1,
                0, 1, 0, 0, 0,
                0, 1, 0, 1, 0,
                0, 0, 1, 1, 1,
                0, 0, 1, 1, 0
        }, 5).findShapes();
        t.print();
        Assert.assertEquals(t.getShapeCount(), 3);
    }

    @Test
    public void testWinding() {
        ShapesTest t = new ShapesTest(new int[]{
                0, 1, 1, 0, 1,
                0, 0, 1, 0, 0,
                1, 1, 1, 0, 1,
                1, 0, 0, 0, 1,
                1, 1, 1, 0, 1
        }, 5).findShapes();
        t.print();
        Assert.assertEquals(t.getShapeCount(), 3);
    }
}

