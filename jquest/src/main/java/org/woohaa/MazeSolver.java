package org.woohaa;

import java.util.*;
import java.util.stream.IntStream;

public class MazeSolver {
    public static final char BLOCKED = 'X';
    public static final char OPEN = '-';
    private static final char CHECKED = '?';
    private static final char PATH = '*';

    private final char[] search;
    private final int width;
    private final int exit;
    private Optional<List<Integer>> solution = Optional.empty();

    /**
     * constructor to solve the maze
     *
     * @param maze  0 or 1s indicating passable squares
     * @param width width of a row
     */
    public MazeSolver(String maze, int width, int exit) {
        this.width = width;
        this.search = maze.toCharArray();
        this.exit = exit;
    }

    public MazeSolver solve(int start) {
        if (test(start)) {
            Collections.reverse(solution.get());
        }
        return this;
    }

    private boolean booya(int i) {
        if (!solution.isPresent()) {
            solution = Optional.of(new ArrayList<>());
        }
        search[i] = PATH;
        solution.get().add(i);
        return true;
    }

    private boolean test(int i) {
        // guard: must be a valid cell
        if (i < 0 || i >= search.length) {
            return false;
        }

        // see if we made it
        if (i == exit) {
            return booya(i);
        }

        // guard: must not be blocked
        if (search[i] != OPEN) {
            return false;
        }

        // mark this place
        search[i] = CHECKED;

        // check in each direction for true == path
        return ((i - width >= 0 && test(i - width))
                || (i + width < search.length && test(i + width))
                || (i % width > 0 && test(i - 1))
                || (i % width < width - 1 && test(i + 1)))
                && booya(i);
    }

    public Optional<List<Integer>> getSolution() {
        return solution;
    }

    public void print() {
        StringBuffer b = new StringBuffer(search.length * 2);
        IntStream.range(0, search.length).forEach(i -> {
            b.append(search[i]).append(" ");
            if (i % width == width - 1) {
                b.append("\n");
            }
        });
        if (solution.isPresent()) {
            System.out.println("Solution: " + solution.get());
        } else {
            System.out.println("Nope");
        }
        System.out.println(b);
    }
}
