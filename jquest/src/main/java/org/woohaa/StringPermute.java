package org.woohaa;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class StringPermute {
    private final String s;

    public StringPermute(String s) {
        this.s = s;
    }

    public Set<String> permute() {
        return combine(s.length());
    }

    public Set<String> combine(int length) {
        return addCombinations(new HashSet<>(),"", s, length);
    }

    private String sort(String s) {
        char[] cc = s.toCharArray();
        Arrays.sort(cc);
        return new String(cc);
    }

    /**
     * choosing ignores order in the combinations
     * @return the results of choosing
     */
    public Set<String> choose(int length) {
        Set<String> s = combine(length);
        return s.stream().map(ss -> sort(ss)).collect(Collectors.toSet());
    }

    private Set<String> addCombinations(Set<String> all, String s, String r, int length) {
        if (s.length() < length) {
            for (int i = 0; i < r.length(); i++) {
                addCombinations(all,s + r.charAt(i), r.substring(0, i) + r.substring(i + 1, r.length()), length);
            }
        }

        if (s.length() == length) {
            all.add(s);
        }

        return all;
    }
}
