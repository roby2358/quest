package org.woohaa;

import java.util.stream.IntStream;

public class MazeBuilder {
    private static char BLOCKED = 'X';
    private static char OPEN = '-';
    private static char UP = '^';
    private static char DOWN = 'v';
    private static char LEFT = '<';
    private static char RIGHT = '>';
    private static char ALL = '*';

    private final char[] maze;
    private final int width;
    private final int start;
    private final int end;
    private final RandStack<Integer> stack = new RandStack<>();

    /**
     * create a new maze, start by filling in the maze with blocked
     *
     * @param width
     * @param height
     */
    public MazeBuilder(int width, int height, int start, int end) {
        this.width = width;
        this.maze = new char[width * height];
        IntStream.range(0, width * height).forEach(i -> this.maze[i] = BLOCKED);
        this.start = start;
        this.end = end;
    }

    private boolean dig(int i, int ii, char d) {
        if (maze[ii] == BLOCKED) {
            maze[i] = OPEN;
            maze[ii] = d;
            stack.push(ii);
        }
        return true;
    }

    public MazeBuilder build() {
        stack.push(start);
//        stack.push(end);

        maze[start] = ALL;
//        maze[end] = ALL;

        // now pop and build until no more options
        while (!stack.isEmpty()) {
            int i = stack.rpop();

            if (maze[i] == ALL) {
                if (i % width > 0) dig(i, i - 1, LEFT);
                if (i % width < width - 1) dig(i, i + 1, RIGHT);
                if (i - width >= 0) dig(i, i - width, UP);
                if (i + width < maze.length) dig(i, i + width, DOWN);
            } else if (maze[i] == LEFT) {
                if (i % width > 0) dig(i, i - 1, ALL);
            } else if (maze[i] == RIGHT) {
                if (i % width < width - 1) dig(i, i + 1, ALL);
            } else if (maze[i] == UP) {
                if (i - width >= 0) dig(i, i - width, ALL);
            } else if (maze[i] == DOWN) {
                if (i + width < maze.length) dig(i, i + width, ALL);
            }
        }

        IntStream.range(0, maze.length).forEach(i -> {
            if (maze[i] != OPEN) maze[i] = BLOCKED;
        });

        maze[end] = OPEN;

        return this;
    }

    public String toString() {
        return new String(maze);
    }

    public void print() {
        StringBuffer b = new StringBuffer(maze.length * 2);
        b.append("\t\"");
        IntStream.range(0, maze.length).forEach(i -> {
            b.append(maze[i]);
            if (i % width == width - 1) b.append("\"\n" + "\t+ \"");
        });
        System.out.println(b);
    }

}
