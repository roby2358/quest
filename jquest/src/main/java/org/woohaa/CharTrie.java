package org.woohaa;

import java.util.Optional;
import java.util.stream.IntStream;

public class CharTrie {
    private CharTrie[] trie = new CharTrie[127];

    private int toAsciiInt(char c) {
        return ((int) c) & 127;
    }

    /**
     * add an ascii string to the trie
     * @param s the string to add
     */
    public void add(String s) {
        CharTrie tt = this;

        for (int i = 0; i < s.length(); i++) {
            int ii = toAsciiInt(s.charAt(i));

            if (tt.trie[ii] == null) {
                tt.trie[ii] = new CharTrie();
            }
            tt = tt.trie[ii];
        }
    }

    /**
     * check to see if a string is in our trie
     */
    public boolean check(String s) {
        CharTrie tt = this;

        for (int i = 0; i < s.length() ; i++) {
            int ii = toAsciiInt(s.charAt(i));
            if (tt.trie[ii] == null) {
                return false;
            }
            tt = tt.trie[ii];
        }

        // made it!
        return true;
    }

    public Optional<CharTrie> checkNext(char c) {
        int ii = toAsciiInt(c);
        return Optional.ofNullable(trie[ii]);
    }
}
