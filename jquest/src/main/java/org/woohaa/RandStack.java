package org.woohaa;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * operations to push an item onto the queue, and pop a random item off
 * @param <T>
 */
public class RandStack<T> {
    private final List<T> q = new ArrayList<>();
    private final Random random = new Random();

    /**
     * add a new item to the queue
     * @param i the item to add
     * @return this
     */
    public RandStack push(T i) {
        q.add(i);
        return this;
    }

    /**
     * "pop" an element and smash the list down
     *
     * <p>
     *     Must check isEmpty before calling
     * </p>
     *
     * @return a random item from the queue
     */
    public T rpop() {
        int i = random.nextInt(q.size());
        T r = q.get(i);

        // smash and remove the last one
        IntStream.range(i, q.size() - 1).forEach(ii -> q.set(ii, q.get(ii + 1)));
        q.remove(q.size() - 1);

        return r;
    }

    public boolean isEmpty() {
        return q.isEmpty();
    }
}
