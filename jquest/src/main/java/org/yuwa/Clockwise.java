package org.yuwa;

public class Clockwise {

    private static final char[][] chars =
            {{'a', 'b', 'c', 'd'},
            {'e', 'f', 'g', 'h'},
            {'i', 'j', 'k', 'l'},
            {'m', 'n', 'o', 'p'}};

    private StringBuffer despool(char[][] chars, int a, int b, int c, int d, StringBuffer out) {
        if ( c < a || d < b) {
            // done
        }
        else {
            // top row
            for (int i = a; i <= c; i++) {
                out.append(chars[b][i]);
            }
            // right side
            for (int j = b + 1; j <= d; j++) {
                out.append(chars[j][c]);
            }
            // bottom
            for (int k = c - 1; k >= a; k--) {
                out.append(chars[d][k]);
            }
            // left
            // bottom
            for (int l = d - 1; l > a; l--) {
                out.append(chars[l][a]);
            }
            despool(chars, a + 1, b + 1, c - 1, d - 1, out);
        }
        return out;
    }

    public static void main(String[] args) {
        StringBuffer out = new StringBuffer();
        new Clockwise().despool(chars, 0, 0, chars[0].length - 1, chars.length - 1, out);
        System.out.println(out);
    }
}
