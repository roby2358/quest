package org.yuwa

import org.scalatest.{FlatSpec, Matchers}

import scala.util.Random

class MaxHeapSpec extends FlatSpec with Matchers {

  val r = new Random()

  it must "add an element" in {
    val mh = new MaxHeap()
    mh.add(2).heap.toVector shouldBe Vector(2)
    mh.check shouldBe true
    mh.add(3).heap.toVector shouldBe Vector(3, 2)
    mh.check shouldBe true
    mh.add(4).heap.toVector shouldBe Vector(4, 2, 3)
    mh.check shouldBe true
    mh.add(1).heap.toVector shouldBe Vector(4, 2, 3, 1)
    mh.check shouldBe true
    mh.add(0).heap.toVector shouldBe Vector(4, 2, 3, 1, 0)
    mh.check shouldBe true
  }

  it must "pop an element" in {
    val mh = new MaxHeap().add(2, 4, 1, 3, 0)

    mh.pop shouldBe 4
    mh.heap shouldBe Vector(3, 2, 1, 0)
    mh.pop shouldBe 3
    mh.heap shouldBe Vector(2, 0, 1)
    mh.pop shouldBe 2
    mh.heap shouldBe Vector(1, 0)
    mh.pop shouldBe 1
    mh.heap shouldBe Vector(0)
    mh.pop shouldBe 0
    mh.heap shouldBe Vector()
  }

  it must "build and pop a bunch of random elements" in {
    val bh = new MaxHeap()

    // add 100 random and check
    bh.add((0 until 100).map(i => { r.nextInt(100) }): _*)
    bh.check shouldBe true

    println("heapified", bh.heap)

    // pop 100
    val s = (0 until 100).foldLeft(Seq[Int]())( (a, v) => {
      bh.check shouldBe true
      a :+ bh.pop
    })

    println("sorted", s)

    // make s
    bh.check shouldBe true
    bh.heap.isEmpty shouldBe true
    s.sortBy(-_) shouldBe s
  }

  it must "sort in place" in {
    val s = new MaxHeap().add(5, 2, 3, 1, 4, 0).sort
    println("sorted", s)
    s.sorted shouldBe s
  }

  it must "reheapify" in {
    val bh = new MaxHeap()
    bh.heap ++= (0 until 100).map(_ => { r.nextInt(100) })
    bh.reheapify
    println("reheapify", bh.heap)
    bh.check shouldBe true

    bh.sort
    bh.reheapify
    println("reheapify", bh.heap)
    bh.check shouldBe true
  }
}
