package org.yuwa

import org.scalatest.{FlatSpec, Matchers}

import scala.util.Random

class FiboSearch2Spec extends FlatSpec with Matchers {

  val random = new Random()

  def randomSeq(size: Int): IndexedSeq[Int] =
    IndexedSeq.fill(size)(random.nextInt(size * 2)).sorted

  it must "find the right pair" in {
    FiboSearch2(randomSeq(0)).nextFibonacci(1, 1) shouldBe(1, 1)
    FiboSearch2(randomSeq(1)).nextFibonacci(1, 1) shouldBe(1, 1)
    FiboSearch2(randomSeq(3)).nextFibonacci(1, 1) shouldBe(3, 2)
    FiboSearch2(randomSeq(7)).nextFibonacci(1, 1) shouldBe(5, 3)
    FiboSearch2(randomSeq(9)).nextFibonacci(1, 1) shouldBe(8, 5)
    FiboSearch2(randomSeq(12)).nextFibonacci(1, 1) shouldBe(8, 5)
    FiboSearch2(randomSeq(13)).nextFibonacci(1, 1) shouldBe(13, 8)
    FiboSearch2(randomSeq(14)).nextFibonacci(1, 1) shouldBe(13, 8)
  }

  it must "find it" in {
    (0 to 1000).foreach(size => {
      val fs = FiboSearch2(randomSeq(size).toSet.toIndexedSeq.sorted)
      fs.list.indices.foreach(i => {
        fs.find(fs.list(i)) shouldBe Some(i)
      })
      fs.find(-1) shouldBe None
      fs.find(Int.MaxValue) shouldBe None
    })
  }

}
