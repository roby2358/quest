package org.yuwa

import org.scalatest.{FlatSpec, Matchers}

class MagicSquareSpec extends FlatSpec with Matchers {

  it must "recognize a magic square" in {
    MagicSquare.isOne(IndexedSeq(4, 9, 2, 3, 5, 7, 8, 1, 6)) shouldBe true
    MagicSquare.isOne(IndexedSeq(4)) shouldBe true
  }

  it must "find all" in {
    println("IndexedSeq(" +
      MagicSquare.findAll.map(s => s.mkString(", ")).mkString("),\nIndexedSeq(") +
      ")")
  }
}
