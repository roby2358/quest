package org.yuwa

import org.scalatest.{FlatSpec, Matchers}

import scala.util.Random

@SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements"))
class ContigSumSpec extends FlatSpec with Matchers {

//  it must "find it" in {
//    ContigSum(IndexedSeq(1, 3, 2, 6, 3, 4, 9, 5)).findIt(7) shouldBe true
//  }
//
//  it must "not find it" in {
//    ContigSum(IndexedSeq(1, 4, 2, 6, 4, 4, 10, 6)).findIt(9) shouldBe false
//  }

  it must "find random" in {
    val s = ContigSum(IndexedSeq.fill(20){ Random.nextInt(10) * (Random.nextInt(9) + 1) })
    val size = Random.nextInt(5) + 1
    val i = Random.nextInt(s.ar.size - size)
    val sum = s.ar.slice(i, i + size).sum
    s.findItSimple(sum * 1000 + 1) shouldBe false
    s.findItSimple(s.ar.sum) shouldBe true
    s.findItSimple(sum) shouldBe true
  }
}
