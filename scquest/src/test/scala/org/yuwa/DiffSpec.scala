package org.yuwa

import org.scalatest.{FlatSpec, MustMatchers}

class DiffSpec extends FlatSpec with MustMatchers {

  "isDifferent" must "differ with None" in {
    Diff(None, 2).isDifferent mustBe true
  }

  it must "differ" in {
    Diff(Some(1), 2).isDifferent mustBe true
  }

  it must "not differ" in {
    Diff(Some(3), 3).isDifferent mustBe false
  }
}
