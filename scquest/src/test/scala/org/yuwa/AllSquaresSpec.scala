package org.yuwa

import org.scalatest.{FlatSpec, Matchers}

@SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements"))
class AllSquaresSpec extends FlatSpec with Matchers {

  it must "find least" in {
    AllSquares(1, 1).findFirst(0, 1) shouldBe 1
    AllSquares(2, 5).findFirst(0, 2) shouldBe 2
    AllSquares(5, 10).findFirst(0, 5) shouldBe 3
    AllSquares(17, 26).findFirst(0, 17) shouldBe 5
    AllSquares(50, 65).findFirst(0, 50) shouldBe 8
    AllSquares(9, 20).findFirst(0, 9) shouldBe 3
    AllSquares(26, 36).findFirst(0, 9) shouldBe 6
  }

//  it must "find all" in {
//    AllSquares(1, 1).findSquares shouldBe Seq()
//    AllSquares(2, 5).findSquares shouldBe Seq(2)
//    AllSquares(5, 10).findSquares shouldBe Seq(3)
//    AllSquares(17, 26).findSquares shouldBe Seq(5)
//    AllSquares(50, 65).findSquares shouldBe Seq(8)
//    AllSquares(9, 20).findSquares shouldBe Seq(3, 4)
//    AllSquares(26, 36).findSquares shouldBe Seq(6)
//    AllSquares(1, 25).findSquares shouldBe Seq(1, 2, 3, 4, 5)
//  }
//
//  it must "count all" in {
//    AllSquares(1, 1).countSquares shouldBe 1
//    AllSquares(2, 5).countSquares shouldBe 1
//    AllSquares(5, 10).countSquares shouldBe 1
//    AllSquares(17, 26).countSquares shouldBe 1
//    AllSquares(50, 65).countSquares shouldBe 1
//    AllSquares(9, 20).countSquares shouldBe 2
//    AllSquares(26, 36).countSquares shouldBe 1
//    AllSquares(1, 25).countSquares shouldBe 5
//  }
}
