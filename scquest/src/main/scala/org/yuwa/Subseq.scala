package org.yuwa

@SuppressWarnings(Array("org.wartremover.warts.TraversableOps",
  "org.wartremover.warts.NonUnitStatements"))
object Subseq {

  type Answer = (Seq[String], Int)

  val memo = scala.collection.mutable.Map[State, Option[Answer]]()

  final case class State(s: Seq[String], t: Seq[String], words: Seq[String], len: Int) {
    def answer: Answer = (words ++ s, len)

    def noMatch: State = State(s.tail, t, words :+ s.head, if (len == 0) len else len + 1)

    def consumeIt: State = State(s.tail, t.tail, words, len + 1)

    def skipIt: State = State(s.tail, t, words :+ s.head, if (len == 0) len else len + 1)
  }

  def take(state: State): Option[(Seq[String], Int)] = {
    if (state.t.isEmpty)
    // we've used up all our words
      Some(state.answer)
    else if (state.s.isEmpty)
    // nothing left to match!
      None
    else if (memo.contains(state))
    // we've seen this before
      memo.getOrElse(state, None)
    else if (state.s.head != state.t.head)
    // easy, take it and move on
      take(state.noMatch)
    else {
      val all = Seq(state.consumeIt, state.skipIt).map(take)

      println("best of", all)

      val answer = all.maxBy(aa => {
        aa.fold(Int.MinValue)(_._2)
      })

      memo.put(state, answer)

      answer
    }
  }

  def longest(s: Seq[String], t: Seq[String]): Seq[String] = {
    val (words, length) = take(State(s, t, Seq(), 0)).getOrElse(Seq(), 0)
    println(words, length)
    words
  }

  def main(args: Array[String]): Unit = {
    val s = Seq("a", "b", "c", "b", "d", "e", "b", "d", "x")
    val t = Seq("b", "d")
    println(s, t)
    println(longest(s, t))
  }
}
