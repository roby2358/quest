package org.yuwa

object Fun {

  // Cons

  sealed trait List

  case object Nil extends List

  case class Cons(head: Int, tail: List) extends List

  def build(v: Int*): List = v.reverse.foldLeft(Nil.asInstanceOf[List])((a, vv) => {
    Cons(vv, a)
  })

  def fold(zero: Int, op: (Int, Int) => Int)(list: List): Int =
    list match {
      case Nil => zero
      case Cons(head, tail) => op(head, fold(zero, op)(tail))
    }

  def unfold(isEnd: Int => Boolean,
             op: Int => (Int, Int))(seed: Int): List =
    if (isEnd(seed))
      Nil
    else {
      val (head, next) = op(seed)
      Cons(head, unfold(isEnd, op)(next))
    }

  def add(a: Int, b: Int): Int = a + b

  def isZero(x: Int): Boolean = x == 0

  def div10(x: Int): (Int, Int) = (x % 10, x / 10)

  // OCons

  type OCons[R] = Option[(Int, R)]

  def omap[A, B](fun: A => B, ocons: OCons[A]): OCons[B] =
    ocons match {
      case None => None
      case Some((head, tail)) => Some(head, fun(tail))
    }

  def open(list: List): OCons[List] =
    list match {
      case Nil => None
      case Cons(head, tail) => Some((head, tail))
    }

  def close(ocons: OCons[List]): List =
    ocons match {
      case None => Nil
      case Some((head, tail)) => Cons(head, tail)
    }

  def oFold(out: OCons[Int] => Int)(list: List): Int =
    out(omap(oFold(out), open(list)))

  def oUnfold(into: Int => OCons[Int])(seed: Int): List =
    close(omap(oUnfold(into), into(seed)))

  def oAdd(ocons: OCons[Int]): Int =
    ocons match {
      case None => 0
      case Some((head, tail)) => head + tail
    }

  def oDiv10(x: Int): OCons[Int] =
    if (x <= 0)
      None
    else
      Some((x % 10, x / 10))

  // Ind

  trait Functor[F[_]] {
    def map[A, B](fun: A => B, from: F[A]): F[B]
  }

  case class Indirect[Recursive[_]](opt: Recursive[Indirect[Recursive]])

  def irFold[Recursive[_]](ff: Functor[Recursive], out: Recursive[Int] => Int)(ind: Indirect[Recursive]): Int =
    out(ff.map(irFold(ff, out), ind.opt))

  //  def irUnfold[Recursive[_]](ff: Functor[Recursive], into: Int => Recursive[Int])(seed: Int): Indirect[_] =
  //    Indirect(ff.map(irunfold(into), into(seed)))

//  val addFunctor = new Functor[OCons[Int]] {
//    override def map[A, B](fun: A => B, from: OCons[A]): OCons[B] = ???
//  }
//
//  def poong(from: OCons[Int]): Int =
//    from match {
//      case None => 0
//      case Some((head, tail)) => head + tail
//    }

  // Main

  def main(args: Array[String]): Unit = {
    println(build(1, 2, 3))
    println(fold(0, add)(build(1, 2, 3)))
    println(unfold(isZero, div10)(12345))

    println(oFold(oAdd)(build(1, 2, 3)))
    println(oUnfold(oDiv10)(12345))

//    println(irFold[OCons](addFunctor, poong)(Indirect[OCons](build(1, 2, 3))))
  }
}
