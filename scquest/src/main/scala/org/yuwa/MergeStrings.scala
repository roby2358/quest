package org.yuwa

object MergeStrings {

  def merge(a: String, b: String): String = {
    var i = 0
    var j = 0
    var result = ""
    while (i < a.length || j < b.length) {
      if (i < a.length) {
        result += a.charAt(i)
        i += 1
      }
      if (j < b.length) {
        result += b.charAt(j)
        j += 1
      }
    }
    result
  }

  def main(args: Array[String]): Unit = {
    val s0 = "1234"
    val s1 = "abcdefg"

    // our function
    println(merge(s0, s1))

    // use built-ins
    println(s0.zipAll(s1, "", "").map(p => {
      p._1.toString + p._2.toString
    })
      .mkString(""))
  }
}
