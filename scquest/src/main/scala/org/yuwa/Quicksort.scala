package org.yuwa

import scala.collection.mutable
import scala.util.Random

object Quicksort extends App {
  val size = 20
  val list = mutable.ArrayBuffer.fill(size)(Random.nextInt(size * 2))
  val got = Quicksort(list).sortRange(0, list.size)
  println(got)
}

case class Quicksort(list: mutable.ArrayBuffer[Int]) {

  def swap(a: Int, b: Int): Unit = {
    val t = list(a)
    list.update(a, list(b))
    list.update(b, t)
  }

  def sortRange(i: Int, j: Int): mutable.ArrayBuffer[Int] = {

    // Guard: we may be done
    if (i + 1 >= j) return list

    val pivot = list(i)

    // hi is the first index with values gre the pivot
    var hi = j

    // lo is the next value to check
    // every value below lo is less than pivot
    var lo = i + 1

    // traverse the range
    while (lo < hi) {
      if (list(lo) <= pivot)
      // easy
        lo += 1
      else {
        // swap into the other side
        hi -= 1
        swap(lo, hi)
      }
    }

    // move the pivot into place
    swap(i, lo - 1)

    println(list.slice(i, lo - 1).mkString("-"), list(lo - 1), list.slice(lo, j).mkString("+"))
    if (lo > i) {
      sortRange(i, lo - 1)
      sortRange(lo, j)
    }
    return list
  }
}
