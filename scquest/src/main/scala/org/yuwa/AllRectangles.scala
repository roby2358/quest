package org.yuwa

object AllRectangles {
  def main(args: Array[String]): Unit = {
    println(AllRectangles(IndexedSeq((0, 0), (1, 0), (2, 0), (0, 1), (1, 1), (2, 1))).count)
  }
}

case class AllRectangles(points: IndexedSeq[(Int, Int)]) {

  val all: Set[(Int, Int)] = points.toSet

  val sorted: IndexedSeq[(Int, Int)] = points.sorted

  def isRectangle(r: (Int, Int), s: (Int, Int)): Boolean = {
    // println(r, (s._1, r._2), (r._1, s._2), s)
    all.contains((s._1, r._2)) && all.contains((r._1, s._2))
  }

  def lt(r: (Int, Int), s: (Int, Int)): Boolean = r._1 < s._1 && r._2 < s._2

  def count: Int = {

    val rs = for (i <- sorted.indices;
                     j <- i + 1 until sorted.size if lt(sorted(i), sorted(j)) && isRectangle(sorted(i), sorted(j)))
      yield i

    println(rs)

    rs.size
  }
}