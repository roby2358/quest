package org.yuwa

final case class AllSquares(a: Int, b: Int) {

  /**
    * find the lowest perfect square inside a, b
    * So that (n - 1)^2 < a, and n^2 >= a
    *
    * @param lo lowest value, inclusive
    * @param hi highest value, exclusive
    * @return the lowest perfect square inside a,b
    */
  def findFirst(lo: Int, hi: Int): Int = {
    if (a == 0)
      0
    else if (a == 1)
      1
    else if (lo * lo >= a)
      lo
    else if (lo + 1 >= hi)
      hi
    else {
      val m = (lo + hi) / 2
      if (m * m == a)
        m
      else if (m * m < a)
        findFirst(m + 1, hi)
      else
        findFirst(lo, m)
    }
  }

  def findSquares: Seq[Int] = {
    val start = findFirst(0, a)
    (start until b).takeWhile(n => n * n <= b).toVector
  }

//  def countSquares: Int = findSquares match {
//    case v if v.nonEmpty => v.size
//    case _ => 0
//  }

  def countSquares: Int = {
    val r = (math.sqrt(b) + 0.999999999999).toInt - (math.sqrt(a) + 0.99999999).toInt
    if (math.sqrt(b) == math.sqrt(b).toInt)
      r + 1
    else
      r
  }
}
