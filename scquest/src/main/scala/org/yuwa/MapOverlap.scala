package org.yuwa

final case class MapOverlap(available: Seq[String]) {

  val availableCount: Map[String, Int] = toCount(available)

  def toCount(ss: Seq[String]): Map[String, Int] = ss.foldLeft(Map[String, Int]()) { (m, s) =>
    m + (s -> (m.getOrElse(s, 0) + 1))
  }

  def check(need: Array[String]): Boolean = toCount(need).forall { case (k, v) =>
    availableCount.getOrElse(k, 0) == v
  }

}

object MapOverlap {

  def main(args: Array[String]): Unit = {
    println(MapOverlap("two times three is not four".split(" ")).check("two times two is four".split(" ")))
  }

}