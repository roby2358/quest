package org.yuwa

object Votes {

  def count(votes: Array[String]): String  = {
    import scala.collection.mutable

    val map = mutable.Map[String, Int]().withDefaultValue(0)
    votes.foldLeft("") { (max, s) =>

      val c = map.getOrElse(s, 0) + 1
      map.update(s, c)

      if (c > map.getOrElse(max, 0) || (c == map.getOrElse(max, 0) && s > max))
        s
      else
        max
    }
  }

  def main(args: Array[String]): Unit = {
    println(count(Array("Joe", "Mary", "Mary", "Joe")))
  }
}
