package org.yuwa

@SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements",
  "org.wartremover.warts.Null"))
object DivideLinked {

  final case class Item(value: Int, next: Item) {
    def str(n: Item, s: Seq[Int]): Seq[Int] =
      if (n == null)
        s
      else
        str(n.next, s :+ n.value)

    override def toString: String = str(this, Seq()).mkString(" -> ")
  }

  val Empty: Item = null

  /**
    * rofl
    *
    * @param v
    * @return the list as a linked list of Item
    */
  def build(v: Int*): Item = v.reverse.foldLeft(Empty)((a, vv) => {
    Item(vv, a)
  })

  def main(args: Array[String]): Unit = {
    val n = build(6, 2, 3, 5, 4, 1)
    println(n)
    println(DivideLinked(n).divide(3))
  }
}

@SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements",
  "org.wartremover.warts.Null"))
final case class DivideLinked(head: DivideLinked.Item) {

  import DivideLinked._

  def push(a: Item, b: Item): Item = Item(b.value, a)

  def combine(a: Item, b: Item): Item =
    if (b == null)
      a
    else
      combine(push(a, b), b.next)

  def put(x: Int, n: Item, lte: Item, gt: Item): (Item, Item) =
    if (n == null)
      (lte, gt)
    else if (n.value <= x) {
      // add to head
      put(x, n.next, push(lte, n), gt)
    }
    else {
      // add to tail
      put(x, n.next, lte, push(gt, n))
    }

  def divide(x: Int): Item = {
    val lteGt = put(x, head, null, null)
    // unfortunately, since Items are immutable, we have to iterate and rebuild
    // otherwise we could just keep track of the first item & point it at gt
    combine(lteGt._2, lteGt._1)
  }
}
