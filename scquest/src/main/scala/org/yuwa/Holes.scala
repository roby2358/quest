package org.yuwa

object Holes {

  val Holes = scala.collection.Map[Int, Int](1 -> 0, 2 -> 0, 3 -> 0, 5 -> 0, 7 -> 0,
    0 -> 1, 4 -> 1, 6 -> 1, 9 -> 1,
    8 -> 2)

  def countHoles(num: Int): Int =
    num.toString.map(c => { Holes.getOrElse(c.toInt - '0'.toInt, 0) }).sum

  def main(args: Array[String]): Unit = {
    println(countHoles(819))
  }
}
