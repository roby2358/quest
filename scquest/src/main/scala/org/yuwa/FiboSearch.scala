package org.yuwa

import scala.annotation.tailrec
import scala.util.Random

object FiboSearch {

  val rr = new Random()

  def randomSeq(size: Int): IndexedSeq[Int] =
    IndexedSeq.fill(size)(rr.nextInt(size * 2)).sorted

  def main(args: Array[String]): Unit = {
    var r = randomSeq(20)
    val fs = FiboSearch(r)
    val x = rr.nextInt(r.size * 2)
    val got = fs.find(x)
    val dereference = got.fold("nope != ")(i => { r(i) + " == " })
    println(s"""$got $dereference$x""")
  }
}

final case class FiboSearch(list: IndexedSeq[Int]) {

  @tailrec
  private[yuwa] def nextFibonacci(size: Int, fibo0: Int, fibo1: Int): (Int, Int) =
    if (fibo1 >= size)
      (fibo0, fibo1)
    else
      nextFibonacci(size, fibo1, fibo0 + fibo1)

  /**
    * @param size
    * @return the first Fibbonacci number gte size
    */
  val firstFibonacci: (Int, Int) = nextFibonacci(list.size, 0, 1)

  /**
    * @param x
    * @return the optional int of the index of the number, if we found it
    */
  def find(x: Int): Option[Int] = {
    var (f1, f0) = firstFibonacci
    var f2 = f0 - f1
    var offset = -1

    println(list)
    println(x)

    while (f0 > 1) {
      def star(j: Int) = Set(f0, f1, f2).map(_ + offset).contains(j)
      def mark(j: Int): String = if (j == offset) "|" else if (star(j)) "*" else ""
      val showList = list.zipWithIndex.map({ case (v, k) => s"""${mark(k)}$v""" })
      println(showList.mkString(" "))
      println(offset, f2, f1, f0)

      val i = math.min(f2 + offset, list.size - 1)

      println(s"list($i) = ${list(i)}")

      if (list(i) < x) {
        // fall down to f1
        f0 = f1
        f1 = f2
        f2 = f0 - f1
        offset = i
      }
      else if (list(i) > x) {
        // fall down to f2
        f0 = f2
        f1 = f1 - f2
        f2 = f0 - f1
      }
      else
        return Some(i)
    }

    if (f1 > 0 && list(offset + 1) == x)
      Some(offset + 1)
    else
      None
  }

}
