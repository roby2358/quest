package org.yuwa

@SuppressWarnings(Array("org.wartremover.warts.Any",
  "org.wartremover.warts.Var",
  "org.wartremover.warts.Null"))
object MapKeyValue {

  def kv(map: Map[String, Any]): String = map.map{ case (k, v) => s"$k=$v" }.mkString(", ")

  def main(args: Array[String]): Unit = {
    print(kv(Map("a" -> "a", "b" -> 3, "c" -> null)))
  }
}
