package org.yuwa

import scala.annotation.tailrec

final case class ContigSum(ar: IndexedSeq[Int]) {

  final case class Contig(sum: Int, i: Int, j: Int) {
    def extend: Option[Contig] =
      if (j + 1 < ar.size)
        Some(Contig(sum + ar(j + 1), i, j + 1))
      else
        None

    def back: Option[Contig] =
      if (i - 1 >= 0)
        Some(Contig(sum + ar(i - 1), i - 1, j))
      else
        None

    def slide: Option[Contig] =
      if (j + 1 < ar.size)
        Some(Contig(sum - ar(i) + ar(j + 1), i + 1, j + 1))
      else
        None

    def shrink: Option[Contig] =
      if (i + 1 <= j)
        Some(Contig(sum - ar(i), i + 1, j))
      else
        None

    def skip: Option[Contig] =
      if (j + 1 < ar.size)
        Some(Contig(ar(j + 1), j + 1, j + 1))
      else
        None
  }

  @SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements",
    "org.wartremover.warts.Var",
    "org.wartremover.warts.OptionPartial"))
  def findIt(total: Int): Boolean = {
    import scala.collection.mutable

    println(ar)
    println("find", total)

    val start = ar.indices.map { i => Contig(ar(i), i, i) }.filter(_.sum <= total)
    val q = mutable.PriorityQueue[Contig]()(Ordering.by { c: Contig => -math.abs(total - c.sum) })
    q ++= start
    val memo = mutable.Map[Contig, Boolean]()
    var foundIt = false
    var count = 0

    def tryIt(s: Contig): Unit = if (!memo.contains(s)) {
      memo.put(s, s.sum == total)
      q.enqueue(s)
    }

    while (q.nonEmpty && !foundIt) {
      val s = q.dequeue()
      println(s, s.sum == total)
      count += 1

      if (s.sum == total)
      // found it
        foundIt = true
      else {
        memo.put(s, s.sum == total)

        Seq(s.slide, s.back, s.extend).foreach {
          case Some(ss) => tryIt(ss)
          case None => ;
        }
      }
    }
    println("***", ar.size, count)
    foundIt
  }

  @SuppressWarnings(Array("org.wartremover.warts.OptionPartial", "org.wartremover.warts.Var"))
  def findItSimple(total: Int): Boolean = {

    def foundIt(s: Option[Contig]): Boolean = s.fold(false)(_.sum == total)

    @tailrec
    def check(s: Option[Contig]): Option[Contig] = {
      println(s)
      if (s.isEmpty)
        s
      else if (foundIt(s))
        s
      else {
        check(s.flatMap { ss =>
          if (ar(ss.j) > total)
            ss.skip
          else if (ss.sum < total)
            ss.extend
          else
            ss.shrink
        })
      }
    }

    println(ar)
    println("find", total)

    var s = check(Option(Contig(ar(0), 0, 0)))
    foundIt(s)
  }
}
