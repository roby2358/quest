package org.yuwa

case class Diff[T](before: Option[T], after: T) {

  def isDifferent: Boolean = ! before.exists(_ == after)
}
