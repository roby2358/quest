package org.yuwa

object DubReverse {

  def dubReverse(v: Seq[Int]): Seq[Int] = {
    val reverse: Map[Int, Int] = v.zipWithIndex.map { case (v, i) =>
        v - 1 -> i
    }.toMap

    (0 until v.size).map(i => reverse.getOrElse(reverse.getOrElse(i, -1), -1) + 1)
  }

  def main(args: Array[String]): Unit = {
    println(dubReverse(Seq(5, 2, 1, 3, 4)))
  }
}
