package org.yuwa

import scala.annotation.tailrec

object Shortest {

  /**
    * Find the shortest path between 2 nodes with the given color id
    *
    * @param graphNodes number of nodes
    * @param graphFrom  edge start
    * @param graphTo    edge end
    * @param colors     colors of the nodes
    * @param color      color to match
    */
  def findShortest(graphNodes: Int,
                   graphFrom: Array[Int],
                   graphTo: Array[Int],
                   colors: Array[Long],
                   color: Int): Int =
  // pour the edges into a map of lists
    new Shortest(graphFrom, graphTo, colors, color).find

  def main(args: Array[String]): Unit = {
    val l = findShortest(5, Array(1, 1, 2, 3), Array(2, 3, 4, 5), Array(1, 2, 3, 3, 2), 2)
    //    val l = findShortest(4, Array(1, 1, 4), Array(2, 3, 2), Array(1, 2, 1, 1), 1)
    println(l)
  }
}

class Shortest(graphFrom: Array[Int],
               graphTo: Array[Int],
               colors: Array[Long],
               color: Long) {

  import scala.collection.mutable

  type Graph = Map[Int, Set[Int]]

  type Guess = (Path, Set[Int])

  final case class Path(i: Int, j: Int, length: Int) {

    def isOK: Boolean = colors(i - 1) == color || colors(j - 1) == color

    def isDone: Boolean = length > 0 && colors(i - 1) == color && colors(j - 1) == color

    def nextStart(n: Int): Path = Path(math.min(n, j), math.max(n, j), length + 1)

    def nextEnd(n: Int): Path = Path(math.min(i, n), math.max(i, n), length + 1)

    def nextZero(n: Int): Path = Path(n, n, 0)
  }

  val memo: mutable.Set[Path] = mutable.Set()

  //  val ordering: Ordering[Guess] = Ordering.by(e => { e._1.length })

  val queue: mutable.Queue[Guess] = mutable.Queue()

  def available(n: Int, taken: Set[Int]): Set[Int] = graph.getOrElse(n, Set()) -- taken

  val graph: Graph = buildGraph(graphFrom: Array[Int], graphTo: Array[Int])

  /**
    * convert the parallel from / to arrays into a graph of nodes to destinations
    *
    * @param graphFrom sequence of edges from
    * @param graphTo   sequence of edges to
    * @return map of indexes to set of indexes
    */
  def buildGraph(graphFrom: Seq[Int],
                 graphTo: Seq[Int]): Graph =
    graphFrom.indices.foldLeft(mutable.Map[Int, Set[Int]]())((map, i) => {
      val f = graphFrom(i)
      val t = graphTo(i)
      map.put(f, map.getOrElse(f, Set()) + t)
      map.put(t, map.getOrElse(t, Set()) + f)
      map
    }).toMap

  /**
    * Try to extend our path by one
    *
    * @param best the best so far
    * @return best path if exists
    */
  @tailrec
  final def search(best: Option[Path]): Option[Path] =
    if (queue.isEmpty)
      best
    else if (best.isDefined && best.forall(_.length == 1))
    // can't beat 1
      best
    else {
      val (path, taken) = queue.dequeue()

      val next = if (memo.contains(path))
      // Already considered it
        None
      else if (path.length >= best.fold(colors.length)(_.length))
      // no point considering if it's too long
        None
      else if (path.isDone) {
        // done, don't bother extending
        println("got it", path)
        Some(path)
      }
      else if (!path.isOK)
      // just to be safe -- has to start or end on color
        None
      else {
        // consider extending the path
        val nextStart = available(path.i, taken)
        val nextEnd = available(path.j, taken)

        println(path, nextStart, nextEnd, taken)

        if (nextStart.isEmpty && nextEnd.isEmpty)
        // no where else to go
          None
        else {
          // build all the possible paths
          nextStart.foreach(n => {
            // try jumping from the start node
            queue.enqueue((path.nextStart(n), taken + path.i + n))
          })

          nextEnd.foreach(n => {
            // try jumping from the end node
            queue.enqueue((path.nextEnd(n), taken + path.j + n))
          })

          // remember it
          memo += path

          // free ride
          best
        }
      }

      search((best, next) match {
        case (Some(b), None) => best
        case (None, Some(n)) => next
        case (Some(a), Some(b)) if b.length < a.length => next
        case (Some(a), Some(b)) => next
        case (None, None) => None
      })
    }


  /**
    * Start the recursive search
    *
    * @return the shortest path
    */
  def find: Int = {
    graph.keys.filter(n => {
      colors(n - 1) == color
    }).foreach(n => queue.enqueue((Path(n, n, 0), Set(n))))

    search(None).fold(-1)(_.length)
  }
}