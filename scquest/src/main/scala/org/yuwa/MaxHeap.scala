package org.yuwa

import scala.annotation.tailrec
import scala.collection.mutable

class MaxHeap {

  type BinHeap = mutable.ArrayBuffer[Int]

  val heap: BinHeap = mutable.ArrayBuffer[Int]()

  private def swap(i: Int, j: Int): BinHeap = {
    val t = heap(i)
    heap.update(i, heap(j))
    heap.update(j, t)
    heap
  }

  private def parent(i: Int): Int = (i - 1) / 2

  private def child0(i: Int): Int = 2 * i + 1

  private def child1(i: Int): Int = 2 * i + 2

  /**
    * swap a value down
    *
    * @param i the index to check
    * @return the next index to check
    */
  @tailrec
  private final def down(i: Int): Unit =
    if (i <= 0)
      ()
    else if (heap(parent(i)) >= heap(i))
      ()
    else {
      swap(i, parent(i))
      down(parent(i))
    }

  /**
    * @param i the index to check
    * @return the next index to check
    */
  @tailrec
  private final def up(i: Int, size: Int): Unit = {
    // Guard: if we're off the end, quit
    if (i >= size)
      ()
    else {
      // put the nodes in order
      val ii = Seq(i, child0(i), child1(i)).filter(_ < size).maxBy(heap(_))

      // if there's a new max, swap it and check it
      if (ii != i) {
        swap(i, ii)
        up(ii, size)
      }
    }
  }

  /**
    * Add a new item to the heap
    *
    * @param v one or more values
    * @return the updated heap
    */
  def add(v: Int*): MaxHeap = {

    v.foreach(vv => {
      heap += vv
      down(heap.size - 1)
    })

    this
  }

  /**
    * pop the min element off the top
    *
    * @return the min element
    */
  def pop: Int = {
    val v = heap(0)

    // move & remove the last one
    heap(0) = heap(heap.size - 1)
    heap.remove(heap.size - 1)

    // push up the values
    up(0, heap.size)

    v
  }

  /**
    * sort the heap in place
    *
    * <p>
    *   Note: the heap is no longer a heap ;)
    * @return the sorted heap as a Seq
    */
  def sort: Seq[Int] = {
    (heap.size - 1 until 0 by -1).foreach(size => {
      swap(0, size)
      up(0, size)
    })
    heap.toSeq
  }

  /**
    * @return this, with the heap restored
    */
  def reheapify: MaxHeap = {
    (0 until heap.size).foreach(size => {
      down(size)
    })
    this
  }

  /**
    * @return true if the heap variant is preserved
    */
  def check: Boolean = !(1 until heap.size).exists(i => {
    heap(parent(i)) < heap(i)
  })
}
