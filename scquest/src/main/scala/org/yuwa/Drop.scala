package org.yuwa

object Drop {

  def main(args: Array[String]): Unit = {
    println(Drop(2, 120).leastWorst)
  }

}

/**
  * Use dynamic programming to scan the decision tree to figure out the
  * least worst case for determining the floor at which an xbox will break
  *
  * @param floors
  */
final case class Drop(xboxes: Int, floors: Int) {

  import scala.collection.mutable

  final case class State(xboxes: Int, floors: Int) {

    /**
      * if it broke, we have to test the n - 1 floors below, with one less xbox
      * @param n floor dropped from
      * @return the new state
      */
    def broke(n: Int): State = State(xboxes - 1, n - 1)

    /**
      * if it didn't break, we have to test all the floors above us
      * @param n floor dropped from
      * @return the new state
      */
    def safe(n: Int): State = State(xboxes, floors - n)
  }

  val memo: mutable.Map[State, Int] = mutable.Map()

  /**
    * recursively find the least worst case for dropping with the number of floors
    * @param state
    * @return list of worst case
    */
  def leastWorst(state: State): Int = (1 to state.floors).view
    .map(f => math.max(drop(state.broke(f)), drop(state.safe(f))))
    .min

  /**
    * @param state the state where the xbox was dropped
    * @return the least worst result
    */
  def drop(state: State): Int = {
    if (memo.contains(state))
    // already calculated it
      memo.getOrElse(state, 0)
    else if (state.xboxes == 0)
    // no xboxs!
      0
    else if (state.xboxes == 1)
    // worst case, drop from each floor
      state.floors
    else if (state.floors < 1)
      0
    else if (state.floors == 1)
      1
    else if (state.floors == 2)
      2
    else {
      // find the least worst case + 1 for our drop
      val drops = 1 + leastWorst(state)

      println(state, drops)

      memo.put(state, drops)

      drops
    }
  }

  def leastWorst: Int = drop(State(xboxes, floors))

}
