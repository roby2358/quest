package org.yuwa

object SubstringAnagrams {

  def main(args: Array[String]): Unit = {
    val sa = SubstringAnagrams("mississippi")
    println(sa.countAll)
  }
}

final case class SubstringAnagrams(s: String) {

  import scala.collection.mutable

  def combo2(n: Int): Int = n * (n - 1) / 2

  /**
    * Note we don't care about the actual anagram pairs, we just want
    * to tally up all the ones that match
    *
    * @return count of substring pairs that are anagrams of each other
    */
  @SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements"))
  def countAll: Int = {
    val todo = mutable.Queue[Sub](Sub(0, 1))
    val done = mutable.Set[Sub]()
    val allCounts = mutable.Map[String, Int]().withDefault(_ => 0)

    while (todo.nonEmpty) {
      val ss = todo.dequeue

      if (done.contains(ss)) {
        // already done
      }
      else if (ss.j > s.size) {
        // past end
      }
      else {
        done.add(ss)
        ss.counts.foreach(n => allCounts(n) += 1)
        println(ss.string, ss.counts)

        todo.enqueue(ss.next)
        todo.enqueue(ss.stretch)
      }
    }
    println(allCounts)
    allCounts.values.foldLeft(0) { (a, n) => a + combo2(n)}
  }

  final case class Sub(i: Int, j: Int) {

    def stretch: Sub = Sub(i, j + 1)

    def next: Sub = Sub(i + 1, i + 2)

    def string: String = s.substring(i, j)

    def emptyCount: mutable.ArrayBuffer[Int] = mutable.ArrayBuffer.fill(26)(0)

    def compact(cc: mutable.ArrayBuffer[Int]): String =
      cc.zipWithIndex.foldLeft("") { case (a, (n, i)) =>
        if (n == 0)
          a
        else
          a + ('a' + i).toChar.toString + n.toString
      }

    def counts: Option[String] = if (i < j && j <= s.size)
      Some(compact(string.foldLeft(emptyCount) { (a, c) =>
        a(c - 'a') += 1
        a
      }))
    else
      None
  }
}
