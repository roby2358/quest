package org.yuwa

import scala.annotation.tailrec
import scala.util.Random

object FiboSearch2 {

  val rr = new Random()

  def randomSeq(size: Int): IndexedSeq[Int] =
    IndexedSeq.fill(size)(rr.nextInt(size * 2)).sorted

  def main(args: Array[String]): Unit = {
    val fs = FiboSearch2(randomSeq(20))
    val x = rr.nextInt(fs.list.size * 2)
    val got = fs.find(x)
    val dereference = got.fold("nope != ")(i => {
      fs.list(i) + " == "
    })
    println(s"""$got $dereference$x""")
  }
}

final case class FiboSearch2(list: IndexedSeq[Int]) {

  @tailrec
  private[yuwa] def nextFibonacci(fibo0: Int, fibo1: Int): (Int, Int) =
    if (fibo0 + fibo1 > list.size)
      (fibo0, fibo1)
    else
      nextFibonacci(fibo0 + fibo1, fibo0)

  /**
    * @param x the value to find
    * @return the optional int of the index of the number, if we found it
    */
  def find(x: Int): Option[Int] = {
    // Guard: don't bother if it's not here
    if (list.isEmpty || x < list(0) || x > list(list.size - 1))
      return None

    // find the 2 Fibonacci numbers that add up to > list size
    var (f0, f1) = nextFibonacci(1, 1)

    // lo defines the index the value must be greater than or equal
    // f0 + f1 is the high index the value must be less than
    var lo = -1

    //    println(list)
    //    println(x, lo, f1, f0)

    while (f0 > 1) {
      //      def star(j: Int) = Set(f0 + f1, f1).map(_ + lo).contains(j)
      //
      //      def mark(j: Int): String = if (j == lo) "|" else if (star(j)) "*" else ""
      //
      //      val showList = list.zipWithIndex.map({ case (v, k) => s"""${mark(k)}$v""" })
      //      println(showList.mkString(" "))
      //      println(lo, f1, f0)

      val i = math.min(f1 + lo, list.size - 1)

      //      println(s"list($i) = ${list(i)}")

      if (x > list(i)) {
        // its in the high segment
        val t = f1
        f1 = f0 - f1
        f0 = t
        lo = i
      }
      else if (x < list(i)) {
        // its in the low segment
        f0 = f0 - f1
        f1 = f1 - f0
      }
      else
        return Some(i)
    }

    //    println("...", lo, f1, f0)

    if (lo >= list.size - 1)
    // nothing left
      None
    else if (list(f0 + lo) == x)
      Some(f0 + lo)
    else if (list(f1 + lo) == x)
      Some(f1 + lo)
    else {
      //      println("... nope")
      None
    }
  }

}
