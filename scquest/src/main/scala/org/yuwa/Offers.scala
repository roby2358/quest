package org.yuwa

object Offers {

  def jobOffers(scores: Array[Int], lowerLimits: Array[Int], upperLimits: Array[Int]): Array[Int] = {
    import scala.collection.mutable

    val offers = mutable.ArrayBuffer.fill(100)(0)

    scores.sorted.foldLeft((offers, 0)) { case ((a, i), s) =>

      var ii = i
      while (s > upperLimits(ii)) {
        ii += 1
      }
      if (s >= lowerLimits(ii) && s <= upperLimits(ii)) {
        offers.update(ii, offers(ii) + 1)
      }
      (offers, ii)
    }

    offers.toArray
  }
}
