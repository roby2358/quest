package org.yuwa

object TakeInput {

  val In = scala.io.StdIn

  val parseNumbers = ".*([0-9]+).+([0-9]+).*".r

  def main(args: Array[String]) = {
    while (true) {
      println("go")

      val line = In.readLine()
      println(line)

      val all = parseNumbers.findFirstMatchIn(line)
      println(all.fold("?")(_.group(1)), all.fold("?")(_.group(2)))
    }
  }
}
