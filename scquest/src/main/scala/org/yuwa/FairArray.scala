package org.yuwa

object FairArray {

  def main(args: Array[String]): Unit = {
    //    print(maxMin(3, Array(10, 100, 300, 200, 1000, 20, 30)))

    //    println(maxMin(4, Array(1,
    //    2,
    //    3,
    //    4,
    //    10,
    //    20,
    //    30,
    //    40,
    //    100,
    //    200)))

    println(maxMin(2, Array(2,
      1,
      2,
      1,
      2,
      1)))
  }

  def maxMin(k: Int, arr: Array[Int]): Int = {
    val s = arr.toSeq.sorted
    val d = for (i <- 0 to s.size - k) yield {
      s(i + k - 1) - s(i)
    }
    d.min
  }

}
