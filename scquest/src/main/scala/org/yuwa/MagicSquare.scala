package org.yuwa

/**
  * finds all the 3x3 magic squares 1-9
  */
object MagicSquare {

  def isOne(square: IndexedSeq[Int]): Boolean =
    Seq(sum(square, 0, 1, 2),
      sum(square, 3, 4, 5),
      sum(square, 0, 3, 6),
      sum(square, 2, 4, 6),
      sum(square, 1, 4, 7),
      sum(square, 6, 7, 8),
      sum(square, 2, 5, 8),
      sum(square, 0, 4, 8))
      .forall(_ == true)

  @SuppressWarnings(Array("org.wartremover.warts.TraversableOps"))
  def sum(square: IndexedSeq[Int], i: Int*): Boolean =
    if (i.exists(_ >= square.size))
    // if we're not there yet, it still *can* be a magic squre
      true
    else
    // we have to check it
      i.foldLeft(0) { (a, ii) => a + square(ii) } == 15

  final case class State(square: IndexedSeq[Int], i: Int, remaining: Set[Int])

  @SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements", "org.wartremover.warts.Var"))
  def findAll: Set[IndexedSeq[Int]] = {
    import scala.collection.mutable

    val q = mutable.Queue[State]()
    val all = mutable.Set[IndexedSeq[Int]]()
    var count = 0

    //    q.push(State(IndexedSeq.fill(9)(0), 0, (1 to 9).toSet))
    q.enqueue(State(IndexedSeq[Int](), 0, (1 to 9).toSet))

    while (!q.isEmpty) {
      val s = q.dequeue()

      count = count + 1
      if (count > 0 && (count % 100000 == 0))
        println(count)

      val yms = isOne(s.square)
      if (!yms) {
        // done
      }
      else if (yms && s.i == 9) {
        all += s.square
      }
      else {
        s.remaining.foreach { n =>
          val next = State(s.square :+ n, s.i + 1, s.remaining - n)
          q.enqueue(next)
        }
      }
    }

    println(count)

    all.toSet
  }

  /**
    * run the finder
    *
    * @param args
    */
  def main(args: Array[String]): Unit = {
    println(findAll.mkString("\n"))
  }
}
