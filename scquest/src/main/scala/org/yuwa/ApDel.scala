package org.yuwa

object ApDel {

  def main(args: Array[String]): Unit = {
    println(new ApDel().appendAndDelete("ashley", "ash", 2))
  }
}

class ApDel {

  /**
    * find the initial common substring
    */
  def initialCommonSubstringIndex(s: String, t: String): Int =
    if (s.length == 0 || t.length == 0)
      0
    else {
      val indexes = (0 until s.length).takeWhile(i => {
        (i < s.length && i < t.length) && s(i) == t(i)
      })
      if (indexes.isEmpty)
        0
      else
        indexes.max
    }

  /***
    * look at how many characters we need to add and delete to see if
    * we can make the strings equal with k operations
    * @return true if we can do it in that many operations
    */
  def appendAndDelete(s: String, t: String, k: Int): Boolean =
    if (s.length + t.length <= k) {
      // or we can just delete and build until we get k
      true
    }
    else {
      val i = initialCommonSubstringIndex(s, t)
      val toDel = s.length - i - 1
      val toAdd = t.length - i - 1
      println(i, toDel, toAdd)
      (toDel + toAdd <= k) && (k - toDel - toAdd) % 2 == 0
    }
}
