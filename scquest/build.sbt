name := "scquest"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.7"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.7" % "test"

//wartremoverErrors ++= Warts.unsafe
