
class ShapeFinder:

    # set up the state
    def __init__(self, grid, width):
        self.grid = grid
        self.width = width
        self.shapeCount = 0
        self.shapes = dict()

    # increment the number of shapes & return it
    def inc(self):
        self.shapeCount += 1
        return self.shapeCount

    # return true if we've already assigned a shape to i
    def done(self, i):
        for (ss, ll) in self.shapes.items():
            if i in ll:
                return True
        return False

    # iterate over the cell and traverse the shapes
    def find_all(self):
        for i in range(0, len(sf.grid)):
            sf.traverse(i, 0)

    # traverse the next cell in the grid
    def traverse(self, i, n):
        # print('{} -> {} {}'.format(i, self.grid[i], n))

        # quit if out of bounds
        if i < 0 or i > len(self.grid):
            # print('oob')
            return

        # quit if already in a shape
        if self.done(i):
            # print('already {}'.format(self.shapes[i]))
            return
        
        # quit if not filled
        if self.grid[i] == '0':
            # print('blank {}'.format(self.grid[i]))
            return

        # see if we need to start a new shape
        nn = n if n != 0 else self.inc()
        # print('shape {}'.format(nn))

        # remember this cell
        self.add(i, nn)

        # check neighbors
        self.traverse(i - self.width, nn)
        self.traverse(i + self.width, nn)
        if i % self.width > 0:
            self.traverse(i - 1, nn)
        if i % self.width < self.width - 1:
            self.traverse(i + 1, nn)

    # thrn the cell -> shape dict into a shape -> cells dict
    def add(self, i, ss):
        if not ss in self.shapes:
            self.shapes[ss] = {i}
        else:
            self.shapes[ss].add(i)

if __name__ == '__main__':
    sf = ShapeFinder(
        '01001'\
        '01000'\
        '01010'\
        '00111'\
        '00110', 5)
    sf.find_all()
    print(sf.shapes)
