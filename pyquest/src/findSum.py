from itertools import combinations

class FindSum:

    def __init__(self, numbers):
        self.numbers = numbers
        self.sarr = sorted(self.numbers)
        self.sums = dict()

    # output any pair in the array that adds to s, or else None
    def findAnySum(self, s):
        # if we've got it, use it
        if s in self.sums:
            return self.sums[s]

        lo = 0
        hi = len(self.numbers) - 1

        while lo < hi:
            t = self.sarr[lo] + self.sarr[hi]

            # every time we calculate a sum, memoize it
            self.sums[t] = (self.sarr[lo], self.sarr[hi])

            if t == s:
                return self.sarr[lo], self.sarr[hi]

            if t > s:
                hi -= 1
            elif t < s:
                lo += 1
        return None

    # return three numbers that add to 0 or None
    def findZeroSum(self):
        # guard: if 3 zeros in the list, done
        if len([a for a in self.numbers if a == 0]) >= 3:
            return 0, 0, 0

        # note that zero must be p + m + m or p + p + m
        p = list(filter(lambda a: a >= 0, self.numbers))
        m = list(filter(lambda b: b <= 0, self.numbers))

        if len(p) == 0 or len(m) == 0:
            return None

        for pp in p:
            s = self.findAnySum(-pp)
            if s:
                return s + (pp,)

        for mm in m:
            s = self.findAnySum(-mm)
            if s:
                return s + (mm,)

        return None

print(FindSum([1, 2, 4, 7, 11, 15, 20]).findAnySum(15))
print(FindSum([0, 2, 3, 6, 7, 8, 11, 14, 20]).findAnySum(15))
print(FindSum([2, 2]).findAnySum(4))
print(FindSum([2]).findAnySum(4))
print(FindSum([1, 2, 4, 7, 11, 15]).findAnySum(5))
print(FindSum([1, 2, 4, 7, 11, 15]).findAnySum(0))
print(FindSum([1, 2, 4, 7, 11, 15]).findAnySum(200))

print(FindSum([2, -3, 1, 3, 0, 0, -10]).findZeroSum())
print(FindSum([-2, 3, -1, 0, 0, -3, 10]).findZeroSum())
print(FindSum([-2, 3, -1, -3, 10]).findZeroSum())
print(FindSum([1, 0, 2, 0, 3, 0]).findZeroSum())
print(FindSum([2, 3, 5, 8, 13, 21]).findZeroSum())
print(FindSum([-1, -2, -3, -5, -7, -11]).findZeroSum())

