
# Creates a Trie data structure, which is a DAG (Directed Acyclic Graph)
# holding an arbitary number of strings in a n-ary tree structure
class Trie:

    # init sets up the children as None
    def __init__(self):
        self.trie = [None] * 127

    # add a string of characters to the trie by creating nodes
    # for each char
    def add(self, s):
        t = self
        for c in s:
            i = ord(c)
            if t.trie[i] is None:
                t.trie[i] = Trie()
            t = t.trie[i]

    # return true if the trie contains the given string
    def contains(self, s):
        t = self
        for c in s:
            i = ord(c)
            if t.trie[i] is None:
                return False
            t = t.trie[i]
        return True

    # extract all the strings in the DAG
    def extract(self):
        def extractNext(t, s):
            any = False

            # check all the child nodes, drilling down if we find any
            for i in range(0, 127):
                if t.trie[i] is not None:
                    any = True
                    extractNext(t.trie[i], s + chr(i))

            # if there were no children, we got a string
            if not any:
                print(s)

        extractNext(self, "")


if __name__ == "__main__":
    t = Trie()
    t.add("zoom")
    t.add("zing")
    t.add("hooya")
    t.add("exceptional")
    t.add("exception")

    print("zoom", t.contains("zoom"))
    print("zoo", t.contains("zoo"))
    print("zoot", t.contains("zoot"))
    print("nope", t.contains("nope"))

    t.extract()
