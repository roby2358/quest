# https://www.hackerrank.com/challenges/crossword-puzzle/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=recursion-backtracking


def crosswordPuzzle(crossword, words):
    pass

def find_spaces(cross):


def add_horz(i, j, cross, word):
    row = cross[i]
    for w in range(len(word)):
        ch = word[w]

        if i + w >= len(crossword[0]):
            return None

        if row[j] == '-' or row[j] == ch:
            row[j] = ch

    # return an updated clone of the crossword


crossword = ["+-++++++++",
             "+-++++++++",
             "+-++++++++",
             "+-----++++",
             "+-+++-++++",
             "+-+++-++++",
             "+++++-++++",
             "++------++",
             "+++++-++++",
             "+++++-++++"]
words = "LONDON;DELHI;ICELAND;ANKARA"
print(crosswordPuzzle(crossword, words))
