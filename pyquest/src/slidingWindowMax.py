class SlidingWindow:

    def __init__(self, numbers):
        self.numbers = numbers

    # find the max sum window of the given size
    def maxSlidingWindow(self, w):
        mx = None
        j = -1
        for i in range(w - 1, len(self.numbers)):
            # max has fallen off the end, find the new one
            if j <= i - w:
                j += 1
                while j <= i and self.numbers[j] < self.numbers[j + 1]:
                    j += 1

            # see if i bigger
            if self.numbers[i] > self.numbers[j]:
                j = i

            mx = (self.numbers[j],) if not mx else mx + (self.numbers[j],)
        return mx


print(SlidingWindow([2, 3, 4, 2, 6, 2, 5, 1]).maxSlidingWindow(3))
print(SlidingWindow([6, 4, 6, 5, 1, 1, 1]).maxSlidingWindow(3))
