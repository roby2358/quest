import random

width = 20
height = 20

OPEN = ' '
WALL = 'X'

maze = [WALL for _ in range(width * height)]


class RStack:

    def __init__(self):
        self._s = list()

    def push(self, e):
        self._s.append(e)

    def rpop(self):
        r = random.randint(0, len(self._s) - 1)
        return self._s.pop(r)

    def fpop(self):
        return self._s.pop(len(self._s) - 1)

    def lpop(self):
        return self._s.pop(0)

    def is_empty(self):
        return len(self._s) == 0


def show(m):
    print((WALL + ' ') * (width + 1))
    for y in range(0, height):
        print(WALL + ' ' + ' '.join(m[0 + y * width:0 + y * width + width]))


def dig(rs, i1, i2):
    """Dig from i to i2 if we can"""
    if i1 < 0 or i1 >= len(maze) or i2 < 0 or i2 >= len(maze):
        # off the maze
        pass
    elif maze[i1] != WALL or maze[i2] != WALL:
        # already dug
        pass
    else:
        maze[i1] = OPEN
        maze[i2] = OPEN
        rs.push(i2)


def dig_horz(rs, i, i1, i2):
    """Checks to see if we dig from i to i2"""
    if int(i / width) != int(i2 / width):
        # wraps around, so not valid
        pass
    else:
        dig(rs, i1, i2)


def dig_vert(rs, i, i1, i2):
    """Checks to see if we dig from i to i2"""
    if i % width != i2 % width:
        # wraps around, so not valid
        pass
    else:
        dig(rs, i1, i2)


rstack = RStack()
# random.randint(2, width / 2) * 2 +
i = random.randint(2, height / 2 - 2) * 2 * width
maze[i] = OPEN
rstack.push(i)

while not rstack.is_empty():
    i = rstack.rpop()
    choices = [
        lambda _: dig_horz(rstack, i, i - 1, i - 2),
        lambda _: dig_horz(rstack, i, i + 1, i + 2),
        lambda _: dig_vert(rstack, i, i - width, i - width * 2),
        lambda _: dig_vert(rstack, i, i + width, i + width * 2)]
    # random.shuffle(choices)
    for c in choices:
        c(0)

show(maze)
