
class Find134:
    def __init__(self, n):
        self.n  = n

    # find out how much we have
    def have(self, a, b, c):
        return a + b * 3 + c * 4

    # find out how much we need
    def need(self, a, b, c):
        return self.n - self.have(a, b, c)

    # find the combinations of 1, 3, and 4 that add up to the number
    def all134(self):
        # all is a list of tuples of counts of 1, 3, 4
        all = []
        for cc in range(int(self.n / 4) + 1):
            for bb in range(int(self.need(0, 0, cc) / 3) + 1):
                all.append((self.need(0, bb, cc), bb, cc))
        return all

    def recurAll(self):
        all = []
        self.r(all, (), self.n)
        return all

    def r(self, all, tt, nn):
        if nn == 0:
            all.append(tt)
            return
        elif nn < 0:
            return

        self.r(all, tt + (1,), nn - 1)
        self.r(all, tt + (3,), nn - 3)
        self.r(all, tt + (4,), nn - 4)

fi = Find134(5)
print(fi.all134())
print(fi.recurAll())

