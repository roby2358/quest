# https://www.hackerrank.com/challenges/sparse-arrays/problem?h_r=internal-search

from collections import Counter
from pprint import pprint


def matchingStrings(strings, queries):
    counter = Counter()
    for s in strings:
        counter[s] += 1
    return '\n'.join([str(counter[q]) for q in queries])


print(matchingStrings(['aba',
                       'baba',
                       'aba',
                       'xzxb'],
                      ['aba',
                       'xzxb',
                       'ab']))
print(matchingStrings([
'def',
'de',
'fgh'
], [
'de',
'lmn',
'fgh']))
