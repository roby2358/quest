
class Max2:

    # set up the fields
    def __init__(self, name, crop, size):
        self.name = name
        self.crop = crop
        self.size = size

    def __repr__(self):
        return "%s %s %d" % (self.name, self.crop, self.size)

    # add an item to the list by replacing any item it's larger than
    def add(self, list, max):
        if len(list) < max:
            list.append(self)
        else:
            a = self
            for i in range(0, min(len(list), max)):
                if a.size > list[i].size:
                    b = list[i]
                    list[i] = a
                    a = b


if __name__ == "__main__":
    print(True)
    d = dict()
    all = [Max2("alpha", "pop", 100),
        Max2("beta", "pop", 200),
        Max2("gamma", "pop", 300),
        Max2("delta", "pop", 400),
        Max2("epsilon", "pop", 500),

        Max2("iota", "swee", 200),
        Max2("rho", "swee", 400),
        Max2("sigma", "swee", 600),
        Max2("psi", "swee", 800),

        Max2("phi", "cha", 300),
        Max2("pi", "cha", 600),
        Max2("omicron", "cha", 900),
        Max2("omega", "cha", 1200),]

    for a in all:
        if not a.crop in d:
            d[a.crop] = []
        a.add(d[a.crop], 2)

    print(d)


