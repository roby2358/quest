
r = 20

OPEN = '.'
WALL = 'X'

grid = [[OPEN for _ in range(r * 3 + 2)] for _ in range(r * 3 + 2)]


def show():
    for y in range(len(grid) - 1, -1, -1):
        print(' '.join(grid[y]))


grid[0][0] = 'A'
grid[r][0] = 'B'
grid[0][r] = 'C'
grid[r][r] = 'D'

x = 0
y = r
d = 3 - 2 * r
while y > x:
    x += 1
    if d > 0:
        y -= 1
        d += 4 * (x - y) + 10
    else:
        d += 4 * x + 6
    grid[y][x] = WALL
    grid[x][y] = WALL

show()
