
class MakeChange:

    # keep track of the total
    def __init__(self, total):
        self.total = total

    # how much we have
    def have(self, q, d, n, p):
        return q * 25 + d * 10 + n * 5 + p

    # how much more we need
    def need(self, q, d, n, p):
        return self.total - self.have(q, d, n, p)

    # iterate through coin combinations to make the total
    def all(self):
        all = []
        for q in range(0, int(self.total / 25) + 1):
            for d in range(0, int(self.need(q, 0, 0, 0) / 10) + 1):
                for n in range(0, int(self.need(q, d, 0, 0) / 5) + 1):
                    all.append([q, d, n, self.need(q, d, n, 0)])
        return all

    # show a list of all
    def show(self, all):
        print("\n".join([str(a) + " " + str(self.have(a[0], a[1], a[2], a[3])) for a in all]))

    # check a list of all combinations to see if they add up to total
    def check(self, all):
        for c in all:
            assert(c[0] * 25 + c[1] * 10 + c[2] * 5 + c[3] == self.total)


if __name__ == "__main__":
    mc = MakeChange(27)
    all = mc.all()
    mc.show(all)
    mc.check(all)


