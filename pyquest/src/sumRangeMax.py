import array

def arrayManipulation(n, queries):
    diff = array.array('i', [0] * (n + 1))
    # print(diff)

    # build the list of ups and downs
    for q in queries:
        a, b, x = q
        diff[a - 1] += x
        diff[b] += -x
        # print(diff)
    # print(diff)

    # run through the diffs and find the max
    maxx = 0
    current = 0
    for i in range(len(diff)):
        current += diff[i]
        if current > maxx:
            maxx = current
    return maxx


print(arrayManipulation(5, [[1, 2, 100], [2, 5, 100], [3, 4, 100]]))
